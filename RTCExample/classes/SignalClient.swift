//
//  SignalClient.swift
//  RTCExample
//
//  Created by Hamed on 12/29/18.
//  Copyright © 2018 hmd. All rights reserved.
//

import Foundation
import WebRTC
import SocketIO
import SwiftyJSON

protocol SignalClientDelegate: class {
    func signalClientDidConnect(_ signalClient: SignalClient)
    func signalClientDidDisconnect(_ signalClient: SignalClient)
    func signalClient(_ signalClient: SignalClient, didReceiveRemoteSdp sdp: RTCSessionDescription)
    func signalClient(_ signalClient: SignalClient, didReceiveCandidate candidate: RTCIceCandidate)
    func signalClient(_ signalClient: SignalClient, onRemoteHangUp : Any?)
    func signalClient(_ signalClient: SignalClient, onCreatedRoom : String)
    func signalClient(_ signalClient: SignalClient, onJoinedRoom : String)
    func signalClient(_ signalClient: SignalClient, onNewPeerJoined : String)
}

struct Message: Codable {
    enum PayloadType: String, Codable {
        case sdp, candidate
    }
    let type: PayloadType
    let payload: String
}

enum MessageType : String {
    case offer
    case answer
    case candidate
}

open class SocketConnection {
    
    open static let `default` = SocketConnection()
    private let manager: SocketManager
    var socket: SocketIOClient
    let serverUrl = "http://192.168.43.103:1794"
    
    private init() {
        manager = SocketManager(socketURL: URL(string: serverUrl)!, config: [.log(true), .compress])
        socket = manager.defaultSocket
    }
}

class SignalClient {
    
    
    let roomName = "hamed"
    
    
    private let socket: SocketIOClient
    weak var delegate: SignalClientDelegate?
    
    init() {
        socket = SocketConnection.default.socket
        
        socket.on(clientEvent: .connect, callback: {(args, _) in
            self.socketDidConnect(socket: self.socket)
        })
        
        socket.on(clientEvent: .disconnect, callback: {(args, _) in
            self.socketDidDisconnect(socket: self.socket, error: nil)
        })
        
        //room created event
        socket.on("created", callback: {(args, _) in
//           print("SignallingClient", "created call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
            self.delegate?.signalClient(self, onCreatedRoom: self.roomName)
        })
        
        //room is full event
        socket.on("full", callback: {(args, _) in
//            print("SignallingClient", "full call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
        })
        
        //peer joined event
        socket.on("join", callback: {(args, _) in
//            print("SignallingClient", "join call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
            self.delegate?.signalClient(self, onNewPeerJoined: self.roomName)
        })
        
        //when you joined a chat room successfully
        socket.on("joined", callback: {(args, _) in
//            print("SignallingClient", "joined call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
            self.delegate?.signalClient(self, onJoinedRoom: self.roomName)
        })
        
        //log event
        socket.on("log", callback: {(args, _) in
//            print("SignallingClient", "log call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
        })
        
        //bye event
        socket.on("bye", callback: {(args, _) in
            self.delegate?.signalClient(self, onRemoteHangUp: args[0])
        })
        
        //messages - SDP and ICE candidates are transferred through this
        socket.on("message") { (args, _) in
//            print("SignallingClient", "message call() called with: args = [" + args.map{$0 as? String ?? ""}.joined(separator: " ") + "]")
            if let data = args[0] as? String{
                if data == "bye"
                {
                    self.delegate?.signalClient(self, onRemoteHangUp: args[0])
                }
            }
            else {
                let json = JSON(args[0])
                switch(json["type"].stringValue)
                {
                case MessageType.offer.rawValue:
                    let sdp = RTCSessionDescription(type: .offer, sdp: json["sdp"].stringValue)
                    self.delegate?.signalClient(self, didReceiveRemoteSdp: sdp)
                case MessageType.answer.rawValue:
                    let sdp = RTCSessionDescription(type: .answer, sdp: json["sdp"].stringValue)
                    self.delegate?.signalClient(self, didReceiveRemoteSdp: sdp)
                case MessageType.candidate.rawValue:
                    let candidate = RTCIceCandidate(sdp: json["candidate"].stringValue, sdpMLineIndex: json["label"].int32Value, sdpMid: json["id"].stringValue)
                    self.delegate?.signalClient(self, didReceiveCandidate: candidate)
                    
                default:
                    break;
                }
            }
        }
        
    }
    func connect() {
        self.socket.connect()
    }
    
    func send(sdp: RTCSessionDescription) {
//        let message = Message(type: .sdp, payload: sdp.jsonString() ?? "")
//        if let dataMessage = try? JSONEncoder().encode(message),
//            let stringMessage = String(data: dataMessage, encoding: .utf8) {
//            self.socket.write(string: stringMessage)
//        }
        let json = JSON(["type":sdp.type == .offer ? MessageType.offer.rawValue : MessageType.answer.rawValue,"sdp":sdp.sdp])
        var data = [Any]()
        data.append(json.rawValue)
        socket.emit("message", with: data)
    }
    
    func send(candidate: RTCIceCandidate) {
//        let message = Message(type: .candidate,
//                              payload: candidate.jsonString() ?? "")
//        if let dataMessage = try? JSONEncoder().encode(message),
//            let stringMessage = String(data: dataMessage, encoding: .utf8){
//            self.socket.write(string: stringMessage)
//        }
        let json = JSON(["type":"candidate", "age": 25, "label":candidate.sdpMLineIndex, "id":candidate.sdpMid!,"candidate":candidate.sdp])
        var data = [Any]()
        data.append(json.rawValue)
        socket.emit("message", with: data)
    }
    
    func emitInitStatement(message : String) {
        print("SignallingClient", "emitInitStatement() called with: event = [" + "create or join" + "], message = [" + message + "]");
        socket.emit("create or join", message);
    }
    
    func emitMessage(message : String) {
        print("SignallingClient", "emitMessage() called with: message = [" + message + "]");
        socket.emit("message", message);
    }
    
//    func emitMessage(message : RTCSessionDescription) {
//    try {
//    Log.d("SignallingClient", "emitMessage() called with: message = [" + message + "]");
//    JSONObject obj = new JSONObject();
//    obj.put("type", message.type.canonicalForm());
//    obj.put("sdp", message.description);
//    Log.d("emitMessage", obj.toString());
//    socket.emit("message", obj);
//    Log.d("vivek1794", obj.toString());
//    } catch (JSONException e) {
//    e.printStackTrace();
//    }
//    }
//
//
//    public void emitIceCandidate(IceCandidate iceCandidate) {
//    try {
//    JSONObject object = new JSONObject();
//    object.put("type", "candidate");
//    object.put("label", iceCandidate.sdpMLineIndex);
//    object.put("id", iceCandidate.sdpMid);
//    object.put("candidate", iceCandidate.sdp);
//    socket.emit("message", object);
//    } catch (Exception e) {
//    e.printStackTrace();
//    }
//
//    }
}


extension SignalClient {
    func socketDidConnect(socket: SocketIOClient) {
        self.emitInitStatement(message: self.roomName)
        self.delegate?.signalClientDidConnect(self)
    }

    func socketDidDisconnect(socket: SocketIOClient, error: Error?) {
        self.delegate?.signalClientDidDisconnect(self)

        // try to reconnect every two seconds
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            print("Trying to reconnect to signaling server...")
            self.socket.connect()
        }
    }

    func socketDidReceiveMessage(socket: SocketIOClient, text: String) {
        guard let data = text.data(using: .utf8),
            let message = try? JSONDecoder().decode(Message.self, from: data) else {
                return
        }

        switch message.type {
        case .candidate:
            if let candidate = RTCIceCandidate.fromJsonString(message.payload) {
                self.delegate?.signalClient(self, didReceiveCandidate: candidate)
            }
        case .sdp:
            if let sdp = RTCSessionDescription.fromJsonString(message.payload) {
                self.delegate?.signalClient(self, didReceiveRemoteSdp: sdp)
            }
        }
    }

    func socketDidReceiveData(socket: SocketIOClient, data: Data) {
    }
    
}

